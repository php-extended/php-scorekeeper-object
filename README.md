# php-extended/php-scorekeeper-object
A scorekeeper decorator that forwards the calls to a simple cache calls.

![coverage](https://gitlab.com/php-extended/php-scorekeeper-object/badges/main/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-scorekeeper-object/badges/main/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-scorekeeper-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Scorekeeper\ScorekeeperCache;

/* @var $cache \Psr\Cache\CacheItemPoolInterface */

$scorekeeper = new ScorekeeperCache($cache);

```

```php

use PhpExtended\Scorekeeper\ScorekeeperSimpleCache;

/* @var $cache \Psr\SimpleCache\CacheInterface */

$scorekeeper = new ScorekeeperSimpleCache($cache);

```

```php

use PhpExtended\Scorekeeper\ScorekeeperLogger;

/* @var $oldKeeper \PhpExtended\Scorekeeper\ScorekeeperInterface */
/* @var $logger \Psr\Log\LoggerInterface */

$scorekeeper = new ScorekeeperLogger($oldKeeper, $logger);

```


## License

MIT (See [license file](LICENSE)).
