<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-scorekeeper-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Scorekeeper\ScorekeeperInterface;
use PhpExtended\Scorekeeper\ScorekeeperLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * ScorekeeperLoggerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Scorekeeper\ScorekeeperLogger
 *
 * @internal
 *
 * @small
 */
class ScorekeeperLoggerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScorekeeperLogger
	 */
	protected ScorekeeperLogger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScorekeeperLogger(
			$this->getMockForAbstractClass(ScorekeeperInterface::class),
			$this->getMockForAbstractClass(LoggerInterface::class),
		);
	}
	
}
