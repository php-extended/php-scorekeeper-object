<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-scorekeeper-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Scorekeeper;

use PhpExtended\Score\IntegerScore;
use PhpExtended\Score\ScoreInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * ScorekeeperSimpleCache class file.
 * 
 * This class represents a scorekeeper that forwards the score calls to a
 * simple cache system.
 * 
 * @author Anastaszor
 */
class ScorekeeperSimpleCache implements ScorekeeperInterface
{
	
	/**
	 * The cache.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	/**
	 * Builds a new ScorekeeperSimpleCache with the given simple cache.
	 * 
	 * @param CacheInterface $cache
	 */
	public function __construct(CacheInterface $cache)
	{
		$this->_cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Scorekeeper\ScorekeeperInterface::getScore()
	 * @throws InvalidArgumentException
	 */
	public function getScore(string $namespace, string $classname, string $fieldname) : ScoreInterface
	{
		$key = $this->getKey($namespace, $classname, $fieldname);
		if($this->_cache->has($key))
		{
			$value = $this->_cache->get($key);
			if($value instanceof ScoreInterface)
			{
				return $value;
			}
		}
		
		return new IntegerScore(0, 1, 0);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Scorekeeper\ScorekeeperInterface::setScore()
	 * @throws InvalidArgumentException
	 */
	public function setScore(string $namespace, string $classname, string $fieldname, ScoreInterface $score) : bool
	{
		return $this->_cache->set($this->getKey($namespace, $classname, $fieldname), $score);
	}
	
	/**
	 * Builds a key from the namespace, classname and fieldname.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $fieldname
	 * @return string
	 */
	protected function getKey(string $namespace, string $classname, string $fieldname) : string
	{
		return (string) \preg_replace('#\\W#', '_', $namespace.'___'.$classname.'___'.$fieldname);
	}
	
}
