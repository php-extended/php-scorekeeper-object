<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-scorekeeper-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Scorekeeper;

use PhpExtended\Score\ScoreInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * ScorekeeperLogger class file.
 * 
 * This class represents a scorekeeper that logs the score calls.
 * 
 * @author Anastaszor
 */
class ScorekeeperLogger implements ScorekeeperInterface
{
	
	/**
	 * The scorekeeper.
	 * 
	 * @var ScorekeeperInterface
	 */
	protected ScorekeeperInterface $_keeper;
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new ScorekeeperLogger with a real keeper and a logger.
	 * 
	 * @param ScorekeeperInterface $keeper
	 * @param LoggerInterface $logger
	 */
	public function __construct(ScorekeeperInterface $keeper, ?LoggerInterface $logger = null)
	{
		$this->_keeper = $keeper;
		if(null === $logger)
		{
			$logger = new NullLogger();
		}
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Scorekeeper\ScorekeeperInterface::getScore()
	 */
	public function getScore(string $namespace, string $classname, string $fieldname) : ScoreInterface
	{
		$this->_logger->info('GET Score call for {namespace}\\{classname}::{fieldname}', [
			'namespace' => $namespace,
			'classname' => $classname,
			'fieldname' => $fieldname,
		]);
		
		return $this->_keeper->getScore($namespace, $classname, $fieldname);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Scorekeeper\ScorekeeperInterface::setScore()
	 */
	public function setScore(string $namespace, string $classname, string $fieldname, ScoreInterface $score) : bool
	{
		$this->_logger->info('SET Score call for {namespace}\\{classname}::{fieldname} : {score}', [
			'namespace' => $namespace,
			'classname' => $classname,
			'fieldname' => $fieldname,
			'score' => $score,
		]);
		
		return $this->_keeper->setScore($namespace, $classname, $fieldname, $score);
	}
	
}
