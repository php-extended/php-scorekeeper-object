<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-scorekeeper-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Scorekeeper;

use PhpExtended\Score\IntegerScore;
use PhpExtended\Score\ScoreInterface;

/**
 * ScorekeeperNoop class file.
 * 
 * This scorekeeper does nothing to store scores.
 * 
 * @author Anastaszor
 */
class ScorekeeperNoop implements ScorekeeperInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Scorekeeper\ScorekeeperInterface::getScore()
	 */
	public function getScore(string $namespace, string $classname, string $fieldname) : ScoreInterface
	{
		return new IntegerScore(0, 1, 0);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Scorekeeper\ScorekeeperInterface::setScore()
	 */
	public function setScore(string $namespace, string $classname, string $fieldname, ScoreInterface $score) : bool
	{
		return false;
	}
	
}
